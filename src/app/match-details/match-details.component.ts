import { Component, OnInit, Input } from '@angular/core';
import { Matches } from '../model/Matches';
import { FixtureServiceService } from '../fixture-service.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common'

@Component({
  selector: 'app-match-details',
  templateUrl: './match-details.component.html',
  styleUrls: ['./match-details.component.css']
})
export class MatchDetailsComponent implements OnInit {

  match: Matches;

  constructor(
    private fixtureService: FixtureServiceService,
    private route: ActivatedRoute,
    private location: Location
  ) { }

  ngOnInit() {
    this.getMatchDetails();
  }

  getMatchDetails(): void {
    const id = + this.route.snapshot.paramMap.get('id');
    this.fixtureService.getMatchDetails(id).subscribe(match => this.match = match);
  }

  goBack(): void {
    this.location.back();
  }
}

