import { Matches } from './Matches';

export class Fixture{
  matches: Matches[];

    constructor(matches?: Matches[]){
        this.matches = matches;
    }
}