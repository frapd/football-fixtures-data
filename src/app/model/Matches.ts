import { Team } from './Team';


export class Matches{
    idMatch: string;
    season: string[];
    utcDate: string; 
    matchday: number;
    homeTeam: Team;
    awayTeam: Team;
    score: string[];

    constructor(idMatch?: string, season?: string[], utcDate?: string, matchday?: number, 
                homeTeam?: Team, awayTeam?: Team, score?: string[]){

        this.idMatch = idMatch;
        this.season = season;
        this.utcDate = utcDate;
        this.matchday = matchday;
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
        this.score = score;
    }
}